# flake8: noqa

from __future__ import annotations

import unittest


class DummyTests(unittest.TestCase):
    """Tests for compound methods"""

    def test_base_import(self):
        import modelbase_pde as pde

        self.assertTrue(True)

    def test_level1_import(self):
        import modelbase_pde.basemodels
        import modelbase_pde.leafmodels
        import modelbase_pde.utils

    def test_level2_import(self):
        import modelbase_pde.utils.coordinates
        import modelbase_pde.utils.perffuncs
        import modelbase_pde.utils.plotting

    def test_from_import(self):
        from modelbase_pde.basemodels import HexGridModel
        from modelbase_pde.leafmodels import HexLeafModel
