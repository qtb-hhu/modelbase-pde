Changelog
=========


0.2.46 (2023-10-16)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.45' into 'main' [Marvin van Aalst]

  0.2.45

  See merge request qtb-hhu/modelbase-pde!46
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.45 (2023-10-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.44' into 'main' [Marvin van Aalst]

  0.2.44

  See merge request qtb-hhu/modelbase-pde!44
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated dependencies. [Marvin van Aalst]


0.2.44 (2023-10-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.43' into 'main' [Marvin van Aalst]

  0.2.43

  See merge request qtb-hhu/modelbase-pde!43
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.43 (2023-09-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.42' into 'main' [Marvin van Aalst]

  0.2.42

  See merge request qtb-hhu/modelbase-pde!42
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.42 (2023-09-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.41' into 'main' [Marvin van Aalst]

  0.2.41

  See merge request qtb-hhu/modelbase-pde!41
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.41 (2023-09-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.40' into 'main' [Marvin van Aalst]

  0.2.40

  See merge request qtb-hhu/modelbase-pde!40
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.40 (2023-09-04)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.39' into 'main' [Marvin van Aalst]

  0.2.39

  See merge request qtb-hhu/modelbase-pde!39
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.39 (2023-08-28)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.38' into 'main' [Marvin van Aalst]

  0.2.38

  See merge request qtb-hhu/modelbase-pde!38
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.38 (2023-08-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.37' into 'main' [Marvin van Aalst]

  0.2.37

  See merge request qtb-hhu/modelbase-pde!37
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.37 (2023-08-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.36' into 'main' [Marvin van Aalst]

  0.2.36

  See merge request qtb-hhu/modelbase-pde!36
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.36 (2023-08-07)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.35' into 'main' [Marvin van Aalst]

  0.2.35

  See merge request qtb-hhu/modelbase-pde!35
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.35 (2023-07-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.34' into 'main' [Marvin van Aalst]

  0.2.34

  See merge request qtb-hhu/modelbase-pde!34
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.34 (2023-07-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.33' into 'main' [Marvin van Aalst]

  0.2.33

  See merge request qtb-hhu/modelbase-pde!33
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.33 (2023-07-17)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.32' into 'main' [Marvin van Aalst]

  0.2.32

  See merge request qtb-hhu/modelbase-pde!32
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.32 (2023-07-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.31' into 'main' [Marvin van Aalst]

  0.2.31

  See merge request qtb-hhu/modelbase-pde!31
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.31 (2023-07-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.30' into 'main' [Marvin van Aalst]

  0.2.30

  See merge request qtb-hhu/modelbase-pde!30
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.30 (2023-06-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.29' into 'main' [Marvin van Aalst]

  0.2.29

  See merge request qtb-hhu/modelbase-pde!29
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.29 (2023-06-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.28' into 'main' [Marvin van Aalst]

  0.2.28

  See merge request qtb-hhu/modelbase-pde!28
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.28 (2023-06-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.27' into 'main' [Marvin van Aalst]

  0.2.27

  See merge request qtb-hhu/modelbase-pde!27
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.27 (2023-06-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.26' into 'main' [Marvin van Aalst]

  0.2.26

  See merge request qtb-hhu/modelbase-pde!26
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.26 (2023-05-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.25' into 'main' [Marvin van Aalst]

  0.2.25

  See merge request qtb-hhu/modelbase-pde!25
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.25 (2023-05-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.24' into 'main' [Marvin van Aalst]

  0.2.24

  See merge request qtb-hhu/modelbase-pde!24
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.24 (2023-05-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.23' into 'main' [Marvin van Aalst]

  0.2.23

  See merge request qtb-hhu/modelbase-pde!23
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.23 (2023-05-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.22' into 'main' [Marvin van Aalst]

  0.2.22

  See merge request qtb-hhu/modelbase-pde!22
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.22 (2023-04-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.21' into 'main' [Marvin van Aalst]

  0.2.21

  See merge request qtb-hhu/modelbase-pde!21
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.21 (2023-03-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.20' into 'main' [Marvin van Aalst]

  0.2.20

  See merge request qtb-hhu/modelbase-pde!20
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.20 (2023-03-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.19' into 'main' [Marvin van Aalst]

  0.2.19

  See merge request qtb-hhu/modelbase-pde!19
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.19 (2023-03-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.18' into 'main' [Marvin van Aalst]

  0.2.18

  See merge request qtb-hhu/modelbase-pde!18
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.18 (2023-03-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.17' into 'main' [Marvin van Aalst]

  0.2.17

  See merge request qtb-hhu/modelbase-pde!17
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.17 (2023-02-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.16' into 'main' [Marvin van Aalst]

  0.2.16

  See merge request qtb-hhu/modelbase-pde!16
- Dev: chg: removed unused import. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.16 (2023-02-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.15' into 'main' [Marvin van Aalst]

  0.2.15

  See merge request qtb-hhu/modelbase-pde!15
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.15 (2023-01-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.14' into 'main' [Marvin van Aalst]

  0.2.14

  See merge request qtb-hhu/modelbase-pde!14
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.14 (2023-01-23)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.13' into 'main' [Marvin van Aalst]

  0.2.13

  See merge request qtb-hhu/modelbase-pde!13
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.13 (2023-01-16)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.12' into 'main' [Marvin van Aalst]

  0.2.12

  See merge request qtb-hhu/modelbase-pde!12
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.12 (2023-01-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.11' into 'main' [Marvin van Aalst]

  0.2.11

  See merge request qtb-hhu/modelbase-pde!11
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.11 (2023-01-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.10' into 'main' [Marvin van Aalst]

  0.2.10

  See merge request qtb-hhu/modelbase-pde!10
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.10 (2023-01-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.9' into 'main' [Marvin van Aalst]

  0.2.9

  See merge request qtb-hhu/modelbase-pde!9
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.9 (2022-12-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.8' into 'main' [Marvin van Aalst]

  0.2.8

  See merge request qtb-hhu/modelbase-pde!8
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.8 (2022-12-12)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.7' into 'main' [Marvin van Aalst]

  0.2.7

  See merge request qtb-hhu/modelbase-pde!7
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.7 (2022-11-29)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.6

  See merge request qtb-hhu/modelbase-pde!6
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.6 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated python version bound. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.5

  See merge request qtb-hhu/modelbase-pde!5
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.5 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.4

  See merge request qtb-hhu/modelbase-pde!4
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.4 (2022-11-14)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.3

  See merge request qtb-hhu/modelbase-pde!3
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.3 (2022-11-07)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  Dependencies

  See merge request qtb-hhu/modelbase-pde!2
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated ci container. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: old. [Marvin van Aalst]
- Dev: chg: added badges to readme. [Marvin van Aalst]
- Dev: chg: fixed poetry build. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.1

  See merge request qtb-hhu/modelbase-pde!1
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.1 (2022-09-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: added changelog to dev dependencies. [Marvin van Aalst]


0.1.0 (2022-09-26)
------------------
- Dev: chg: changed to poetry build. [Marvin van Aalst]
- Dev: chg: proper layout. [Marvin van Aalst]
- Dev: new: added .gitattributes. [Marvin van Aalst]
- Updated version number. [Marvin van Aalst]
- Added some tests. [Marvin van Aalst]
- Fixed setup.py. [Marvin van Aalst]
- Added import tests. [Marvin van Aalst]
- Added modelbase dependency. [Marvin van Aalst]
- Added test backbone. [Marvin van Aalst]
- Initial commit. [Marvin van Aalst]


