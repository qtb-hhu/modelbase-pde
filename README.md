# modelbase pde

[![pipeline status](https://gitlab.com/marvin.vanaalst/modelbase-pde/badges/main/pipeline.svg)](https://gitlab.com/marvin.vanaalst/modelbase-pde/-/commits/main)
[![coverage report](https://gitlab.com/marvin.vanaalst/modelbase-pde/badges/main/coverage.svg)](https://gitlab.com/marvin.vanaalst/modelbase-pde/-/commits/main)
[![PyPi](https://img.shields.io/pypi/v/modelbase-pde)](https://pypi.org/project/modelbase-pde/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![Downloads](https://pepy.tech/badge/modelbase-pde)](https://pepy.tech/project/modelbase-pde)

Subpackage of the [modelbase](https://gitlab.com/ebenhoeh/modelbase) package.
